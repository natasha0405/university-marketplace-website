/**
 *
 *      +-----------------------------------------------------------------------------------------------------------------+ 
 *      |                                             Copyright - NWMSU                                                   |
 *      |                                           NWMSU INTERNAL USE ONLY                                               |
 *      |-----------------------------------------------------------------------------------------------------------------| 
 *      |Team 05                                                                                                          |
 *      |Unit 05 : Purna Medarametla , Lakshman Pavan Kumar Vudutha                                                                                   |
 *      |Unit 08 : Chinta Raja Srikar Karthik, Chandar Mouli Kantipudi                                                    |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Description: This program uses mocha and node-mocks to test "user" controller.                                   |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |    NAME                            VERSION                       CHANGES                                        |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Chinta Raja Srikar Karthik       0.0.1(Initial)       Initial code added  to test user controllers.              |
 *      |                                                                                                                 |             
 *      |                                                                                                                 |
 *      +-----------------------------------------------------------------------------------------------------------------+
 */
process.env.NODE_ENV = 'test'
const app = require('../../app.js')
var User = require('../../models/user.js')
const LOG = require('../../utils/logger.js')
const mocha = require('mocha')
const expect = require('chai').expect
var request = require('supertest')
const EMAIL = 'test@test.com'
const PASSWORD = 'Testing'

LOG.debug('Starting test/controllers/user.js.')

mocha.describe('API Tests - User Controller', function () {
    mocha.describe('GET /login',function(){

    })
    mocha.beforeEach('GET /login',function(){
        
    })
    mocha.it('find a user by email',function(){

    })

    mocha.describe('GET /signup', function () {
        var token

    })
})