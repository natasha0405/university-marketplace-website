/**
 *
 *      +-----------------------------------------------------------------------------------------------------------------+ 
 *      |                                             Copyright - NWMSU                                                   |
 *      |                                           NWMSU INTERNAL USE ONLY                                               |
 *      |-----------------------------------------------------------------------------------------------------------------| 
 *      |Team 04                                                                                                          |
 *      |Unit 04 : Prem Kiran Osipalli  , ujjawal Kumar                                                                   |
 *      |Unit 09 : Ashok Atkuri, Venkata Siva Sai Nadipeneni, Sai Kumar Uppala                                            |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Description: This program uses mocha and node-mocks to test "user" model.                                        |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |    NAME                            VERSION                       CHANGES                                        |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Ashok Atkuri       0.0.1(Initial)       Initial code added  to test user model.                                  |
 *      |                                                                                                                 |             
 *      |                                                                                                                 |
 *      +-----------------------------------------------------------------------------------------------------------------+
 */

var expect = require('chai').expect;

var User = require('../../models/user');

describe('User', function() {
    it('should be valid if email is empty', function(done) {
        var u = new User();        
        u.validate(function(err) {
            expect(u.email).to.exist;
            done();
        });
    });
});

describe('User', function() {
	it('should be invalid if password is empty', function(done) {
		var u = new User();
		u.validate(function(err) {
			expect(u.password).to.exist;
			done();
		});
	});
});

describe('User', function() {
	it('should be invalid if password is empty', function(done) {
		var u = new User();
		u.validate(function(err) {
			expect(u.password).to.exist;
			done();
		});
	});
});

describe('estimate', function() {
    it('should be invalid if email is String', function(done) {
        var m = new User();
 
        m.validate(function(err) {
            expect(m.email).to.be.a('String');
            done();
        });
    });
});

describe('estimate', function() {
    it('should be invalid if email is NUll', function(done) {
        var m = new User();
 
        m.validate(function(err) {
            expect(m.email).not.to.be.null;
            done();
        });
    });
});
