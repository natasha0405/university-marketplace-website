/*This is main.js file*/
// function toCurrency(amount) 

// https://gist.github.com/kylefox/780065
function toCurrency(amount) {
    return "$" + amount.toFixed(2);
};

// function toCurrencyInt(amount) 
function toCurrencyInt(amount) {
    return "$" + amount.toFixed(0);
};

// Add  .toCurrency() method to all Numbers
Number.prototype.toCurrency = function () {
    return toCurrency(this);
};

// Add  .toCurrencyInt() method to all Numbers
Number.prototype.toCurrencyInt = function () {
    return toCurrencyInt(this);
};

function onLaborChange() {
        var newlaborcost = $('#numPeople').val() * $('#numDays').val() *
            $('#hoursWorked').val() * $('#laborHourlyCharge').val();
        $("#laborcost").html(newlaborcost);
        onFoodChange();               TotalCost();
    }

    function onHotleChange() {
        var newhotelcost = $('#numrooms').val() * $('#numnights').val() *
            $('#roomcost').val();
        $("#hotelsum").html(newhotelcost);        TotalCost();
    }
    function onFoodChange() {
        var people=$('#numPeople').val();
        var days=$('#numDays').val();        
        $("#numPeopleFood").val(people);
        $("#numDaysFood").val(days);
        var newfoodcost = people*days * $('#foodcostperday').val() ;
     $("#foodsum").html(newfoodcost);              TotalCost(); 
    }
    function onVehicleChange() {
        var newvehiclecost = $('#numberOfVehicles').val() * $('#milesPerVehicle').val() *
            $('#dollarsPerMile').val();
        $("#vehiclecost").html(newvehiclecost);
        TotalCost();
    }


    function TotalCost() {
        var Totalcost = parseInt($('#materialsum').html())+ parseInt($('#laborcost').html()) +
        parseInt($('#foodsum').html()) +parseInt($('#vehiclecost').html())+
        parseInt($('#miscellaneoussum').html()) ;
        $("#totalcost").html(Totalcost);
        $("#totalcostpersqft").html(Math.round(Totalcost/ $('#sqft').val()));
    }


function onsqftChange() {
    const newsqft = parseFloat($("input[name=sqft]").val());
    let price = 75
    if (newsqft == 1) {
        price = 100
    }
    $("#estimateCost").html(price.toCurrencyInt());

}

// window.onload = a function that will call all update functions to display calculated values
window.onload = function () {
    onsqftChange();
}

// prevent submission when hitting enter key - optional, but user-friendly 
document.forms[0].onkeypress = function (e) {
    var key = e.charCode || e.keyCode || 0;
    if (key == 13) {
        e.preventDefault();
    }
}
var counter = 1;
function addInput(divName) {
    var newdiv = document.createElement('div');
    if (divName == "dynamicMiscellaneousInput") {
        newdiv.innerHTML = "<div class=row><div class='col col-xs-6'><div class='form-group'><input type='text' class='form-control' id='MiscellaneousName<%=iEntry%>' name='misc' value=''></div></div><div class='col col-xs-6'><div class='form-group'><input type='text' class='form-control' id='MiscellaneousName<%=iEntry%>' name='miscCost' value=''></div></div></div>";
    } else if (divName == 'dynamicMaterialInput') {
        newdiv.innerHTML = "<div class=row><div class='col col-xs-4'><div class='form-group'><input type='text' class='form-control' id='MaterialName<%=iEntry%>' name='materialName' value=''></div></div><div class='col col-xs-4'><div class='form-group'><input type='text' class='form-control' id='MaterialName<%=iEntry%>' name='materialUnitCost' value=''></div></div><div class='col col-xs-4'><div class='form-group'><input type='text' class='form-control' id='MaterialName<%=iEntry%>' name='materialSqft' value=''></div></div></div>";
    }
    document.getElementById(divName).appendChild(newdiv);
    counter++;
}
  // END CLIENT SIDE CODE  (close the script tag)